import csv
import json
from itertools import product
from pathlib import Path
import shutil

from coast.cli import search, report, retrieve, compare

wd = Path("..")
sequences_folder = wd / "sequences"
taxonomic_filter_folders = wd / "taxonomic_filters"
data_folder = wd / "data"

# taxonomic_filters = "10239_2021.06.11_12.34.25.txids"

query_names = ["MN908947.3.gb", "MN844877.gb", "NC_016071.gb"]
# engine = ["blast"]
db = ["UniProtKB_SwissProt", "UniProtKB_Trembl"]

test_conditions = (i for i in list(product(query_names, db)))


def validation(query_name, db, sequences, taxonomic_filter, data):
    query = sequences / query_name
    workspace = data / db / query.stem
    search_result = workspace / "blast_results.tab"
    report_result = workspace / "coast_results.tab"
    #
    report([query], results_path=search_result, output_path=workspace, out_fmt=["b", "a", "r"])

    retrival_results = workspace / "retrieval"
    # retrival_results.mkdir()

    # retrieve(report_result, output_path=retrival_results, id_table=True)
    p = retrival_results.glob('**/*.faa')
    # shutil.copyfile(workspace / "protein.faa", (workspace / ("query_" + query_name)).with_suffix(".faa"))

    retrived_files = [(workspace / ("query_" + query_name)).with_suffix(".faa")]
    retrived_files.extend([x for x in p if x.is_file()])

    compare_results = workspace / "compare"
    # compare_results.mkdir()

    # compare(retrived_files, compare_results, output_path=compare_results)
    report(retrived_files, compare_results / "coast_aln.tab", output_path=compare_results, reciprocal=True,
           out_fmt=["b", "a", "r"])


def benchmark(test_cases):
    for case in test_cases:
        print(case)
        yield validation(*case, sequences_folder, taxonomic_filter_folders, data_folder)


validation_results = [result for result in benchmark(test_conditions)]
